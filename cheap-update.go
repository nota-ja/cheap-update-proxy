package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"text/template"
)

var (
	listen = fmt.Sprintf("0.0.0.0:%s", os.Getenv("PORT"))

	scriptDir = func() string {
		dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			log.Println(err)
		}
		return (dir + "/")
	}()

	beforeStart = false
)

func main() {
	given := os.Getenv("CU_BEFORE_START")
	if given != "" {
		if err := generateScript("cu-before-start.sh", given); err != nil {
			log.Println(err)
		}
		beforeStart = true
	}

	given = os.Getenv("CU_START")
	if given != "" {
		if err := generateScript("cu-start.sh", given); err != nil {
			log.Println(err)
		}
		if err := start(nil); err != nil {
			log.Println(err)
		}
	}

	proxyHandler := http.HandlerFunc(proxyHandlerFunc)
	http.ListenAndServe(listen, proxyHandler)
}

func proxyHandlerFunc(w http.ResponseWriter, r *http.Request) {
	log.Printf("Scheme:%s\n", r.URL.Scheme)
	log.Printf("Opaque:%s\n", r.URL.Opaque)
	log.Printf("User:%s\n", r.URL.User)
	log.Printf("Host:%s\n", r.URL.Host)
	log.Printf("Path:%s\n", r.URL.Path)
	log.Printf("RawQuery:%s\n", r.URL.RawQuery)
	log.Printf("Fragment:%s\n", r.URL.Fragment)

	if r.URL.Path == "/__TEST" {
		w.Write([]byte("OK\n"))
		return
	} else if r.URL.Path == "/__ENV" {
		envs := os.Environ()
		for _, env := range envs {
			w.Write([]byte(env + "\n"))
		}
		return
	} else if r.URL.Path == "/__UPDATE_RESTART" {
		err := updateRestart()
		var res string
		if err != nil {
			res = fmt.Sprintf("Error in updateRestart: %s\n", err)
			log.Printf(res)
		} else {
			res = "OK\n"
		}
		w.Write([]byte(res))
		return
	}

	client := &http.Client{}

	r.RequestURI = ""
	r.URL.Scheme = "http"
	r.URL.Host = "127.0.0.1:1271"

	resp, err := client.Do(r)
	if err != nil {
		res := fmt.Sprintf("Error in proxying: %s", err)
		log.Printf(res)
		w.Write([]byte(res))
		return
	}
	resp.Write(w)
}

func updateRestart() (err error) {
	log.Printf("PATH=%s\n", os.Getenv("PATH"))

	err = update()
	if err != nil {
		return err
	}

	err = restart()
	if err != nil {
		return err
	}

	return nil
}

func update() (err error) {
	log.Printf("SCM_BRANCH=%s\n", os.Getenv("SCM_BRANCH"))

	err = runCommand(true, false, scriptDir+"cu-update.sh")
	if err != nil {
		return err
	}

	return nil
}

func restart() (err error) {
	fp, err := os.OpenFile(scriptDir+"run/cu-app.pid", os.O_RDWR, 0600)
	if err != nil {
		log.Println(err)
		return err
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)
	scanner.Scan()
	if err = scanner.Err(); err != nil {
		log.Println(err)
		return err
	}
	pid := scanner.Text()

	if pid != "" {
		err = runCommand(true, false, "kill", "-SIGINT", pid)
		if err != nil {
			log.Println(err)
		}
	}

	if err = start(fp); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func start(fp *os.File) (err error) {
	if beforeStart {
		if err = runCommand(true, false, scriptDir+"cu-before-start.sh"); err != nil {
			log.Println(err)
			return err
		}
	}

	if err = runCommand(false, false, scriptDir+"cu-start.sh"); err != nil {
		log.Println(err)
		return
	}

	return nil
}

func runCommand(waitFinish bool, ignoreError bool, name string, arg ...string) error {
	cmd := exec.Command(name, arg...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	var err error
	if waitFinish {
		err = cmd.Run()
	} else {
		err = cmd.Start()
	}
	if err != nil {
		log.Println(err)
		if ignoreError {
			err = nil
		}
	}

	return err
}

func generateScript(scriptFilename string, givenScript string) (err error) {
	script_template := template.Must(template.ParseFiles(scriptFilename + ".tpl"))

	dict := map[string]string{
		"given": givenScript,
	}
	var buf bytes.Buffer

	if err = script_template.Execute(&buf, dict); err != nil {
		return
	}
	script := buf.String()

	fp, err := os.Create(scriptDir + scriptFilename)
	if err != nil {
		return
	}
	defer fp.Close()

	if err = fp.Truncate(0); err != nil {
		return
	}

	writer := bufio.NewWriter(fp)
	if _, err = writer.WriteString(script); err != nil {
		return
	}
	writer.Flush()

	fp.Close()
	exec.Command("chmod", "700", scriptDir+scriptFilename).Run()

	return nil
}
