#!/bin/bash

set -e # fail fast
set -x # print traces

droplet_base_dir=$(cd $(dirname $0) && cd .. && pwd)

pushd ${droplet_base_dir}/app
git fetch origin
git checkout ${SCM_BRANCH}
git reset --hard origin/$SCM_BRANCH
git submodule sync --recursive
git submodule update --init --recursive
popd
