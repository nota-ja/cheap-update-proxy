#!/bin/bash

set -e # fail fast
set -x # print traces

droplet_base_dir=$(cd $(dirname $0) && cd .. && pwd)
pidfile=${droplet_base_dir}/app/run/cu-app.pid
envfile=${droplet_base_dir}/logs/env.log

if [ ! -e $envfile ]; then
    cat /proc/$(cat ${droplet_base_dir}/run.pid)/environ | perl -e '$_=<>; s/\0/\n/g; print $_' > $envfile
fi
source $envfile

umask 077

pushd ${droplet_base_dir}/app

{{.given}} &
started=$!
echo "$started" > $pidfile
if [ -e $pidfile ]; then
    echo "PID file: OK"
    cat $pidfile
else
    echo "PID file: NG"
    exit 1
fi
wait $started

popd
